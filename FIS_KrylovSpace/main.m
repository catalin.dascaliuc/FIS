%% GMRES Data import & conversion
[A, ~] = convert_to_csr('Auxiliary/orsirr_1.mtx');

%% Run GMRES restarted investigation
n = length(A.I)-1;
x_star = ones(n,1);
b = mv_csr(A,x_star);
x0 = zeros(n,1);
opt = 3;
m_vec = [30, 50, 100, 150, 200];
for idx = 1:length(m_vec)
m = m_vec(idx);
tic
[xm, ro, r0_norm, iter] = restarted_GMRES(A,x0,b,m,opt);
t_end = toc
t_vec(idx) = t_end;
fig1 = figure;
iter_vec(idx) = iter;
semilogy(0:iter,[r0_norm,ro]/r0_norm);
xlabel('Iteration number')
ylabel('Relative residual l2 norm')
title(['Restarted GMRES m=',num2str(m)])
grid on
saveas(fig1,[num2str(m),'Res_GMRES.emf'])
end
t_vec = t_vec;
%% Run GMRES full
n = length(A.I)-1;
x_star = ones(n,1);
b = mv_csr(A,x_star);
x0 = zeros(n,1);
opt = 0;
tic
m = length(x0);
[xm, ro, r0_norm, iter, Vj] = full_GMRES(A,x0,b,m,opt,0);
toc
fig1 = figure;
semilogy(0:iter,[r0_norm,ro]/r0_norm);
xlabel('Iteration number')
ylabel('Relative residual l2 norm')
title('Full GMRES')
grid on
saveas(fig1,'Full_GMRES.emf')

fig1 = figure;
dp_vj = zeros(iter,1);
for i=1:iter
    dp_vj(i) = abs(dot_prod(Vj(:,1),Vj(:,i)));
end
semilogy(2:iter,dp_vj(2:iter));
xlabel('k')
ylabel('abs(v_1,v_k)')
title('Krylov Vectors Orthogonality')
grid on
saveas(fig1,'Krylov_Orth.emf')
%% Run GMRES full with Jacobi precond
n = length(A.I)-1;
x_star = ones(n,1);
b = mv_csr(A,x_star);
x0 = zeros(n,1);
tic
m = length(x0);
opt = 1;
[xm, ro, r0_norm, iter, Vj] = full_GMRES(A,x0,b,m,opt,0);
toc
fig1 = figure;
semilogy(0:iter,[r0_norm,ro]/r0_norm);
xlabel('Iteration number')
ylabel('Relative residual l2 norm')
title('Full GMRES Jacobi')
grid on
saveas(fig1,'Full_GMRES_Jacobi.emf')
%% Run GMRES full with GS precond
tic
m = length(x0);
opt = 2;
[xm, ro, r0_norm, iter, Vj] = full_GMRES(A,x0,b,m,opt,0);
toc
fig1 = figure;
semilogy(0:iter,[r0_norm,ro]/r0_norm);
xlabel('Iteration number')
ylabel('Relative residual l2 norm')
title('Full GMRES Gauss-Seidel')
grid on
saveas(fig1,'Full_GMRES_GS.emf')
%% CG Data import & conversion
[A, ~] = convert_to_csr('Auxiliary/s3rmt3m3.mtx');
%% Run CG investigation
n = length(A.I)-1;
x_star = ones(n,1);
b = mv_csr(A,x_star);
x0 = zeros(n,1);
tic
[xm,iter,err_saved,rm_saved_norm,r0_norm] = CG(A,x0,b);
toc
fig1=figure;
semilogy(0:iter,[r0_norm,rm_saved_norm(1:iter)]/r0_norm);
xlabel('Iteration number')
ylabel('Relative residual l2 norm')
title('CG')
grid on
saveas(fig1,'CG_relative.emf')
fig1 = figure;
semilogy(0:iter,[r0_norm,rm_saved_norm(1:iter)]);
xlabel('Iteration number')
ylabel('||r||_2')
title('CG')
grid on
saveas(fig1,'CG_abs.emf')
fig1=figure;
semilogy(1:iter,err_saved(1:iter));
xlabel('Iteration number')
ylabel('||e||_A')
title('CG')
grid on
saveas(fig1,'CG_err.emf')

%% Functions
function [M, A] = convert_to_csr(filename)
% input -
%       filename - string path to .mtx file
% output -
%       M - structure with CSR format with fields:
%           I - points to index in J
%           J - column index
%           V - values
%           symm - flag if symmetric (1) if not (0)
[A, rows, cols, entries, ~, ~, symm] = mmread(filename);
k = 1;
M.I = zeros(1,rows+1);
M.J = zeros(1,entries);
M.V = zeros(1,entries);
if strcmp(symm, 'general')
    M.symm = 0;
elseif strcmp(symm, 'symmetric')
    M.symm = 1;
end
temp = 0;
for i = 1:rows
    if M.symm == 1
        cols = i;
    end
    for   j = 1:cols
        if A(i,j) ~= 0
            
            M.J(k) = j;
            M.V(k) = full(A(i,j));
            if i ~=temp
                M.I(i) = k;
                temp = i;
            end
            k = k+1;
        end
    end
end
M.I(rows+1) = k;
M.I(M.I==0) = [];
M.J(M.J==0) = [];
M.V(M.V==0) = [];
A = full(A);
end

function dp = dot_prod(v1,v2)
n = length(v1);
dp = 0;
% if n~=length(v2)
%     error('Length of vectors do not coincide')
% end
for i = 1:n
    dp = dp+ v1(i)*v2(i);
end
end

function [vj1,hj] = GetKrylov(A,Vj,M_inv,opt)
j = size(Vj,2);
w = mv_csr(A,Vj(:,j));
N = size(Vj,1);
if opt == 1
    for idx=1:N
    w(idx) = M_inv(idx)*w(idx);
    end
elseif opt == 2
    w = fwSubGS_csr(A,w);
else
end
hj = zeros(j+1,1);
for i = 1:j
    hj(i) = dot_prod(w,Vj(:,i));
    for idx=1:N
        w(idx,1) = w(idx)-hj(i)*Vj(idx,i);
    end
end
hj(j+1) = sqrt(dot_prod(w,w));
vj1 = zeros(N,1);
for idx=1:N
    vj1(idx,1) = w(idx,1)/hj(j+1);
end
hj = hj(:);
end

function [xm, ro_saved, r0_norm, iter, Vj] = full_GMRES(A,x0,b,m,opt,r0_passed)
%GMRES Summary of this function goes here
%   Detailed explanation goes here
Ax0 = mv_csr(A,x0);
N = length(x0);
r0 = zeros(N,1);
for idx =1:N
    r0(idx) = b(idx)-Ax0(idx);
end
M_inv = 0;
if opt == 1
    M_inv = Jacobi(A);
    for idx=1:N
        r0(idx) = M_inv(idx)*r0(idx);
    end
elseif opt == 2
    r0 = fwSubGS_csr(A,r0);  
end
r0_norm = sqrt(dot_prod(r0,r0));
ro = r0_norm;
v1 = r0/r0_norm;
g(1) = r0_norm;
if opt == 3
    % for restarted GMRES make sure r0_norm corresponds to initial value;
    r0_norm = r0_passed;
end
Vj = v1;
Hm = [];
for j =1:m
    [vj1, hj] = GetKrylov(A,Vj,M_inv,opt);
    Vj = [Vj, vj1];
    Hm(end+1,:) = zeros(1, size(Hm,2));
    Hm = [Hm,hj];
    if j>=2
        for k = 2:j
            temp1 = Hm(k-1,j);
            temp2 = Hm(k,j);
            Hm(k-1,j) = c(k-1)*temp1 + s(k-1)*temp2;
            Hm(k,j) = -s(k-1)*temp1 + c(k-1)*temp2;
        end
    end
    c(j) = Hm(j,j)/sqrt(Hm(j,j)^2+Hm(j+1,j)^2);
    s(j) = Hm(j+1,j)/sqrt(Hm(j,j)^2+Hm(j+1,j)^2);
    Hm(j,j) = c(j)*Hm(j,j)+s(j)*Hm(j+1,j);
    Hm(j+1,j) = 0;
    g(j+1) = -s(j)*g(j);
    g(j) = c(j)*g(j);
    % backsubstitution
    y = zeros(j,1);
    y(j) = g(j)/Hm(j,j);
    if j>=2
        for i = j-1:-1:1
            y(i) = (g(i)-dot_prod(y(i+1:j),Hm(i,i+1:j)))/Hm(i,i);
        end
    end
    xm = zeros(N,1);
    for idx=1:N
        xm(idx) = x0(idx) + dot_prod(Vj(idx,1:end-1),y);
    end
    if opt ==1
        Axm = mv_csr(A,xm);
        rm = zeros(N,1);
        for idx =1:N
            rm(idx) = M_inv(idx)*(b(idx)-Axm(idx));
        end
        ro = sqrt(dot_prod(rm,rm));
    elseif opt == 2
        Axm = mv_csr(A,xm);
        rm = zeros(N,1);
        for idx =1:N
            rm(idx) = (b(idx)-Axm(idx));
        end
        rm = fwSubGS_csr(A,rm);
        ro = sqrt(dot_prod(rm,rm));
    else
        ro = abs(g(j+1));
    end
    ro_saved(j) = ro;
    if ro/r0_norm <10^(-8)
        break
    end
end
iter = j;
end

function x = fwSubGS_csr(A,b)
n = length(b);
x=zeros(n,1);
x(1) = b(1)/A.V(1);
for i=2:n
    s = 0;
    i1 = A.I(i);
    i2 = A.I(i+1)-1;
    temp = A.J(i1:i2);
    id = [];
    for idx = 1:length(temp)
        if temp(idx) == i
            id = idx;
            break
        end
    end
    for idx = 1:id-1
        s = s + A.V(i1+idx-1)*x(A.J(i1+idx-1));
    end
    x(i)=(b(i)-s)/A.V(i1+id-1);
end
end

function [y] = mv_csr(M,x)
%MV_CSR Summary of this function goes here
%   Detailed explanation goes here
n = length(M.I)-1;
y = zeros(1,n);
for i=1:n
    i1 = M.I(i);
    i2 = M.I(i+1)-1;
    y(i) = dot_prod(M.V(i1:i2),x(M.J(i1:i2)));
end
if M.symm == 1
    for i=1:n
        i1 = M.I(i);
        i2 = M.I(i+1)-1;
        y(M.J(i1:i2)) = y(M.J(i1:i2))+M.V(i1:i2)*x(i);
        temp = M.J(i1:i2);
        id = [];
        for idx = 1:length(temp)
            if temp(idx) == i
                id = idx;
                break
            end
        end
        if ~isempty(id)
            y(i) = y(i) - x(i)*M.V(i1+id-1);
        end
    end
end
y = y(:);
end

function M_inv = Jacobi(A)
n = length(A.I)-1;
M_inv = zeros(n,1);
for i=1:n
    i1 = A.I(i);
    i2 = A.I(i+1)-1;
    temp = A.J(i1:i2);
    id = [];
    for idx = 1:length(temp)
        if temp(idx) == i
            id = idx;
            break
        end
    end
    if ~isempty(id)
        M_inv(i) = 1/A.V(i1+id-1);
    end
end
end
function M_GS = GS(A)
n = length(A.I)-1;
M_inv = zeros(n,n);
for i=1:n
    i1 = A.I(i);
    i2 = A.I(i+1)-1;
    temp = A.J(i1:i2);
    id = [];
    for idx = 1:length(temp)
        if temp(idx) == i
            id = idx;
            break
        end
    end
    if ~isempty(id)
        for idx = 1
        M_inv(i,i) = A.V(i1+id-1);
        end
    end
end
end

function [xm, ro_saved_full,r0_norm, iter] = restarted_GMRES(A,x0,b,m,opt)
%RESTARTED_GMRES Summary of this function goes here
%   Detailed explanation goes here
Ax0 = mv_csr(A,x0);
N = length(x0);
r0 = zeros(N,1);
for idx=1:N
    r0(idx) = b(idx)-Ax0(idx);
end
ro = sqrt(dot_prod(r0,r0));
r0_norm = ro;
x = x0;
iter = 0;
ro_saved_full = [];
while ro/r0_norm>10^(-8)
    [xm, ro_saved, ~, iter_temp, ~] = full_GMRES(A,x,b,m,opt,r0_norm);
    ro = ro_saved(end);
    ro_saved_full = [ro_saved_full,ro_saved];
    x = xm;
    iter = iter+iter_temp;
end
end

function [xm,iter,err_saved,rm_saved,r0_norm] = CG(A,xm,b)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
Axm = mv_csr(A,xm);
N = length(xm);
rm = zeros(N,1);
for idx =1:N
    rm(idx) = b(idx)-Axm(idx);
end
pm = rm;
rm_dp = dot_prod(rm,rm);
r0_norm = sqrt(rm_dp);
iter = 0;
iter_max = 90000;
rm_saved = zeros(1,iter_max);
err_saved = zeros(1,iter_max);
err = zeros(N,1);
rm_dp_norm = r0_norm;
while rm_dp_norm/r0_norm>10^(-8)
    Apm = mv_csr(A,pm);
    alfa = rm_dp/dot_prod(Apm,pm);
    for idx = 1:N
        xm(idx) = xm(idx)+alfa*pm(idx);
        rm(idx) = rm(idx)-alfa*Apm(idx);
    end
    rm1_dp = dot_prod(rm,rm);
    beta = rm1_dp/rm_dp;
    for idx =1:N
        pm(idx) = rm(idx)+beta*pm(idx);
    end
    rm_dp = rm1_dp;
    iter = iter+1;
    for idx =1:N
        err(idx) = xm(idx)-1;
    end
    err_A2norm_sqr = dot_prod(mv_csr(A,err),err);
    rm_dp_norm = sqrt(rm_dp);
    rm_saved(iter) = rm_dp_norm;
    err_saved(iter) = sqrt(err_A2norm_sqr);
end
end



function  [A,rows,cols,entries,rep,field,symm] = mmread(filename)
% function from the Matrix Market website
% function  [A] = mmread(filename)
%
% function  [A,rows,cols,entries,rep,field,symm] = mmread(filename)
%
%      Reads the contents of the Matrix Market file 'filename'
%      into the matrix 'A'.  'A' will be either sparse or full,
%      depending on the Matrix Market format indicated by
%      'coordinate' (coordinate sparse storage), or
%      'array' (dense array storage).  The data will be duplicated
%      as appropriate if symmetry is indicated in the header.
%
%      Optionally, size information about the matrix can be
%      obtained by using the return values rows, cols, and
%      entries, where entries is the number of nonzero entries
%      in the final matrix. Type information can also be retrieved
%      using the optional return values rep (representation), field,
%      and symm (symmetry).
%

mmfile = fopen(filename,'r');
if ( mmfile == -1 )
    disp(filename);
    error('File not found');
end;

header = fgets(mmfile);
if (header == -1 )
    error('Empty file.')
end

% NOTE: If using a version of Matlab for which strtok is not
%       defined, substitute 'gettok' for 'strtok' in the
%       following lines, and download gettok.m from the
%       Matrix Market site.
[head0,header]   = strtok(header);  % see note above
[head1,header]   = strtok(header);
[rep,header]     = strtok(header);
[field,header]   = strtok(header);
[symm,header]    = strtok(header);
head1 = lower(head1);
rep   = lower(rep);
field = lower(field);
symm  = lower(symm);
if ( length(symm) == 0 )
    disp(['Not enough words in header line of file ',filename])
    disp('Recognized format: ')
    disp('%%MatrixMarket matrix representation field symmetry')
    error('Check header line.')
end
if ( ~ strcmp(head0,'%%MatrixMarket') )
    error('Not a valid MatrixMarket header.')
end
if (  ~ strcmp(head1,'matrix') )
    disp(['This seems to be a MatrixMarket ',head1,' file.']);
    disp('This function only knows how to read MatrixMarket matrix files.');
    disp('  ');
    error('  ');
end

% Read through comments, ignoring them

commentline = fgets(mmfile);
while length(commentline) > 0 & commentline(1) == '%',
    commentline = fgets(mmfile);
end

% Read size information, then branch according to
% sparse or dense format

if ( strcmp(rep,'coordinate')) %  read matrix given in sparse
    %  coordinate matrix format
    
    [sizeinfo,count] = sscanf(commentline,'%d%d%d');
    while ( count == 0 )
        commentline =  fgets(mmfile);
        if (commentline == -1 )
            error('End-of-file reached before size information was found.')
        end
        [sizeinfo,count] = sscanf(commentline,'%d%d%d');
        if ( count > 0 & count ~= 3 )
            error('Invalid size specification line.')
        end
    end
    rows = sizeinfo(1);
    cols = sizeinfo(2);
    entries = sizeinfo(3);
    
    if  ( strcmp(field,'real') )               % real valued entries:
        
        [T,count] = fscanf(mmfile,'%f',3);
        T = [T; fscanf(mmfile,'%f')];
        if ( size(T) ~= 3*entries )
            message = ...
                str2mat('Data file does not contain expected amount of data.',...
                'Check that number of data lines matches nonzero count.');
            disp(message);
            error('Invalid data.');
        end
        T = reshape(T,3,entries)';
        A = sparse(T(:,1), T(:,2), T(:,3), rows , cols);
        
    elseif   ( strcmp(field,'complex'))            % complex valued entries:
        
        T = fscanf(mmfile,'%f',4);
        T = [T; fscanf(mmfile,'%f')];
        if ( size(T) ~= 4*entries )
            message = ...
                str2mat('Data file does not contain expected amount of data.',...
                'Check that number of data lines matches nonzero count.');
            disp(message);
            error('Invalid data.');
        end
        T = reshape(T,4,entries)';
        A = sparse(T(:,1), T(:,2), T(:,3) + T(:,4)*sqrt(-1), rows , cols);
        
    elseif  ( strcmp(field,'pattern'))    % pattern matrix (no values given):
        
        T = fscanf(mmfile,'%f',2);
        T = [T; fscanf(mmfile,'%f')];
        if ( size(T) ~= 2*entries )
            message = ...
                str2mat('Data file does not contain expected amount of data.',...
                'Check that number of data lines matches nonzero count.');
            disp(message);
            error('Invalid data.');
        end
        T = reshape(T,2,entries)';
        A = sparse(T(:,1), T(:,2), ones(entries,1) , rows , cols);
        
    end
    
elseif ( strcmp(rep,'array') ) %  read matrix given in dense
    %  array (column major) format
    
    [sizeinfo,count] = sscanf(commentline,'%d%d');
    while ( count == 0 )
        commentline =  fgets(mmfile);
        if (commentline == -1 )
            error('End-of-file reached before size information was found.')
        end
        [sizeinfo,count] = sscanf(commentline,'%d%d');
        if ( count > 0 & count ~= 2 )
            error('Invalid size specification line.')
        end
    end
    rows = sizeinfo(1);
    cols = sizeinfo(2);
    entries = rows*cols;
    if  ( strcmp(field,'real') )               % real valued entries:
        A = fscanf(mmfile,'%f',1);
        A = [A; fscanf(mmfile,'%f')];
        if ( strcmp(symm,'symmetric') | strcmp(symm,'hermitian') | strcmp(symm,'skew-symmetric') )
            for j=1:cols-1,
                currenti = j*rows;
                A = [A(1:currenti); zeros(j,1);A(currenti+1:length(A))];
            end
        elseif ( ~ strcmp(symm,'general') )
            disp('Unrecognized symmetry')
            disp(symm)
            disp('Recognized choices:')
            disp('   symmetric')
            disp('   hermitian')
            disp('   skew-symmetric')
            disp('   general')
            error('Check symmetry specification in header.');
        end
        A = reshape(A,rows,cols);
    elseif  ( strcmp(field,'complex'))         % complx valued entries:
        tmpr = fscanf(mmfile,'%f',1);
        tmpi = fscanf(mmfile,'%f',1);
        A  = tmpr+tmpi*i;
        for j=1:entries-1
            tmpr = fscanf(mmfile,'%f',1);
            tmpi = fscanf(mmfile,'%f',1);
            A  = [A; tmpr + tmpi*i];
        end
        if ( strcmp(symm,'symmetric') | strcmp(symm,'hermitian') | strcmp(symm,'skew-symmetric') )
            for j=1:cols-1,
                currenti = j*rows;
                A = [A(1:currenti); zeros(j,1);A(currenti+1:length(A))];
            end
        elseif ( ~ strcmp(symm,'general') )
            disp('Unrecognized symmetry')
            disp(symm)
            disp('Recognized choices:')
            disp('   symmetric')
            disp('   hermitian')
            disp('   skew-symmetric')
            disp('   general')
            error('Check symmetry specification in header.');
        end
        A = reshape(A,rows,cols);
    elseif  ( strcmp(field,'pattern'))    % pattern (makes no sense for dense)
        disp('Matrix type:',field)
        error('Pattern matrix type invalid for array storage format.');
    else                                 % Unknown matrix type
        disp('Matrix type:',field)
        error('Invalid matrix type specification. Check header against MM documentation.');
    end
end

%
% If symmetric, skew-symmetric or Hermitian, duplicate lower
% triangular part and modify entries as appropriate:
%

if ( strcmp(symm,'symmetric') )
    A = A + A.' - diag(diag(A));
    entries = nnz(A);
elseif ( strcmp(symm,'hermitian') )
    A = A + A' - diag(diag(A));
    entries = nnz(A);
elseif ( strcmp(symm,'skew-symmetric') )
    A = A - A';
    entries = nnz(A);
end

fclose(mmfile);
end