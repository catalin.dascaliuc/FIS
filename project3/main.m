%% Task1: Test power iter nos6
[A, ~] = convert_to_csr('Auxiliary/nos6.mtx');
n = length(A.I)-1;
x = ones(n,1);
opt_csr = 1;
tol = 10^-8;
for i = 1:n
    x(i) = x(i)/sqrt(n);
end
tic
[lambda_kn6,lambda_diff_vecn6,itern6,~] = power_iter(A,x,tol,opt_csr);
t_end_n6 = toc;
figure;
semilogy(1:itern6,lambda_diff_vecn6(1:itern6), 'LineWidth', 1.5)
title('Convergence plot for nos6.mtx')
ylabel('|\lambda^{(k)}-\lambda^{(k-1)}|')
xlabel('iterations')
legend('eigenvalue difference')
grid on
set(gca, 'FontName', 'Arial','FontSize', 11)

%% Task2: Power iteration analysis for different tol
[A, ~] = convert_to_csr('Auxiliary/s3rmt3m3.mtx');
%%
n = length(A.I)-1;
x = ones(n,1);
lambda_1 = 9.5986080894852857E+03;
for i = 1:n
    x(i) = x(i)/sqrt(n);
end
tol = [10^-2, 10^-4, 10^-6, 10^-10];
opt_csr = 1;
t_end_P2 = zeros(length(tol),1);
lambda_k_P = zeros(length(tol),1);
for idx = 1:length(tol)
    tic
    [lambda_k_P(idx),lambda_diff_P,iter_P,lambda_k_vec_P] = power_iter(A,x,tol(idx),opt_csr);
    t_end_P2(idx) = toc;
    error_l = zeros(iter_P,1);
    for idx2=1:iter_P
        error_l(idx2) = abs(lambda_k_vec_P(idx2)-lambda_1);
    end
    figure;
    semilogy(1:iter_P,error_l, 'LineWidth', 1.5)
    title('Error plot for Power Iteration');
    ylabel('error |\lambda^{(k)}-\lambda_1|')
    xlabel('iterations')
    grid on
    set(gca, 'FontName', 'Arial','FontSize', 11)
    legend(['tol = ', num2str(tol(idx))])
    
    figure;
    semilogy(linspace(0,t_end_P2(idx),length(error_l)),error_l, 'LineWidth', 1.5)
    title('Error plot for Power Iteration');
    ylabel('error |\lambda^{(k)}-\lambda_1|')
    xlabel('Elapsed time in s')
    grid on
    set(gca, 'FontName', 'Arial','FontSize', 11)
    legend(['tol = ', num2str(tol(idx))])
end

%% Task2: Lanczos method analysis
[A, ~] = convert_to_csr('Auxiliary/s3rmt3m3.mtx');
%%
n = length(A.I)-1;
x = ones(n,1);
lambda_1 = 9.5986080894852857E+03;
for i = 1:n
    x(i) = x(i)/sqrt(n);
end
m = [30, 50, 75, 100];
tol = [10^-2, 10^-4, 10^-6, 10^-10];
opt_csr = 0;
t_end_L = zeros(length(m),1);
t_end_P = zeros(length(m),1);
t_end_LP = zeros(length(m),1);
lambda_k_L = zeros(length(m),1);
for idx = 1:length(m)
    tic
    T = lanczos(A,x,m(idx));
    t_end_L(idx) = toc;
    x1 = ones(m(idx),1);
    for i = 1:m(idx)
        x1(i) = x1(i)/sqrt(m(idx));
    end
    tic
    [lambda_k_L(idx),lambda_diff_L,iter_L,lambda_k_vec_L] = power_iter(T,x1,tol(idx),opt_csr);
    t_end_P(idx) = toc;
    t_end_LP(idx) = t_end_L(idx)+t_end_P(idx);
end

error_PI = zeros(length(lambda_k_P),1);
error_LM = zeros(length(lambda_k_L),1);
error_PI_perc = zeros(length(lambda_k_P),1);
error_LM_perc = zeros(length(lambda_k_L),1);

for idx2=1:length(error_PI)
    error_PI(idx2) = abs(lambda_k_P(idx2)-lambda_1);
    error_PI_perc(idx2) = error_PI(idx2)/lambda_1*100;
end

for idx2=1:length(error_PI)
    error_LM(idx2) = abs(lambda_k_L(idx2)-lambda_1);
    error_LM_perc(idx2) = error_LM(idx2)/lambda_1*100;

end

figure
semilogy(t_end_P2, error_PI, 'LineWidth', 1.5)
hold on
semilogy(t_end_LP, error_LM, 'LineWidth', 1.5)
title('Comparison of Power Iteration and Lanczos method');
ylabel('final error |\lambda^{(k_{final})}-\lambda_1|')
xlabel('Overall runtime in s')
legend({'power iteration','Lanczos method'})
grid on
set(gca, 'FontName', 'Arial','FontSize', 11)

%% Functions
function [M, A] = convert_to_csr(filename)
% input -
%       filename - string path to .mtx file
% output -
%       M - structure with CSR format with fields:
%           I - points to index in J
%           J - column index
%           V - values
%           symm - flag if symmetric (1) if not (0)
[A, rows, cols, entries, ~, ~, symm] = mmread(filename);
k = 1;
M.I = zeros(1,rows+1);
M.J = zeros(1,entries);
M.V = zeros(1,entries);
if strcmp(symm, 'general')
    M.symm = 0;
elseif strcmp(symm, 'symmetric')
    M.symm = 1;
end
temp = 0;
for i = 1:rows
    if M.symm == 1
        cols = i;
    end
    for   j = 1:cols
        if A(i,j) ~= 0
            
            M.J(k) = j;
            M.V(k) = full(A(i,j));
            if i ~=temp
                M.I(i) = k;
                temp = i;
            end
            k = k+1;
        end
    end
end
M.I(rows+1) = k;
M.I(M.I==0) = [];
M.J(M.J==0) = [];
M.V(M.V==0) = [];
A = full(A);
end

function dp = dot_prod(v1,v2)
n = length(v1);
dp = 0;
% if n~=length(v2)
%     error('Length of vectors do not coincide')
% end
for i = 1:n
    dp = dp+ v1(i)*v2(i);
end
end

function [y] = mv_csr(M,x)
%MV_CSR Summary of this function goes here
%   Detailed explanation goes here
n = length(M.I)-1;
y = zeros(1,n);
for i=1:n
    i1 = M.I(i);
    i2 = M.I(i+1)-1;
    y(i) = dot_prod(M.V(i1:i2),x(M.J(i1:i2)));
end
if M.symm == 1
    for i=1:n
        i1 = M.I(i);
        i2 = M.I(i+1)-1;
        y(M.J(i1:i2)) = y(M.J(i1:i2))+M.V(i1:i2)*x(i);
        temp = M.J(i1:i2);
        id = [];
        for idx = 1:length(temp)
            if temp(idx) == i
                id = idx;
                break
            end
        end
        if ~isempty(id)
            y(i) = y(i) - x(i)*M.V(i1+id-1);
        end
    end
end
y = y(:);
end

function  [A,rows,cols,entries,rep,field,symm] = mmread(filename)
% function from the Matrix Market website
% function  [A] = mmread(filename)
%
% function  [A,rows,cols,entries,rep,field,symm] = mmread(filename)
%
%      Reads the contents of the Matrix Market file 'filename'
%      into the matrix 'A'.  'A' will be either sparse or full,
%      depending on the Matrix Market format indicated by
%      'coordinate' (coordinate sparse storage), or
%      'array' (dense array storage).  The data will be duplicated
%      as appropriate if symmetry is indicated in the header.
%
%      Optionally, size information about the matrix can be
%      obtained by using the return values rows, cols, and
%      entries, where entries is the number of nonzero entries
%      in the final matrix. Type information can also be retrieved
%      using the optional return values rep (representation), field,
%      and symm (symmetry).
%

mmfile = fopen(filename,'r');
if ( mmfile == -1 )
    disp(filename);
    error('File not found');
end;

header = fgets(mmfile);
if (header == -1 )
    error('Empty file.')
end

% NOTE: If using a version of Matlab for which strtok is not
%       defined, substitute 'gettok' for 'strtok' in the
%       following lines, and download gettok.m from the
%       Matrix Market site.
[head0,header]   = strtok(header);  % see note above
[head1,header]   = strtok(header);
[rep,header]     = strtok(header);
[field,header]   = strtok(header);
[symm,header]    = strtok(header);
head1 = lower(head1);
rep   = lower(rep);
field = lower(field);
symm  = lower(symm);
if ( length(symm) == 0 )
    disp(['Not enough words in header line of file ',filename])
    disp('Recognized format: ')
    disp('%%MatrixMarket matrix representation field symmetry')
    error('Check header line.')
end
if ( ~ strcmp(head0,'%%MatrixMarket') )
    error('Not a valid MatrixMarket header.')
end
if (  ~ strcmp(head1,'matrix') )
    disp(['This seems to be a MatrixMarket ',head1,' file.']);
    disp('This function only knows how to read MatrixMarket matrix files.');
    disp('  ');
    error('  ');
end

% Read through comments, ignoring them

commentline = fgets(mmfile);
while length(commentline) > 0 & commentline(1) == '%',
    commentline = fgets(mmfile);
end

% Read size information, then branch according to
% sparse or dense format

if ( strcmp(rep,'coordinate')) %  read matrix given in sparse
    %  coordinate matrix format
    
    [sizeinfo,count] = sscanf(commentline,'%d%d%d');
    while ( count == 0 )
        commentline =  fgets(mmfile);
        if (commentline == -1 )
            error('End-of-file reached before size information was found.')
        end
        [sizeinfo,count] = sscanf(commentline,'%d%d%d');
        if ( count > 0 & count ~= 3 )
            error('Invalid size specification line.')
        end
    end
    rows = sizeinfo(1);
    cols = sizeinfo(2);
    entries = sizeinfo(3);
    
    if  ( strcmp(field,'real') )               % real valued entries:
        
        [T,count] = fscanf(mmfile,'%f',3);
        T = [T; fscanf(mmfile,'%f')];
        if ( size(T) ~= 3*entries )
            message = ...
                str2mat('Data file does not contain expected amount of data.',...
                'Check that number of data lines matches nonzero count.');
            disp(message);
            error('Invalid data.');
        end
        T = reshape(T,3,entries)';
        A = sparse(T(:,1), T(:,2), T(:,3), rows , cols);
        
    elseif   ( strcmp(field,'complex'))            % complex valued entries:
        
        T = fscanf(mmfile,'%f',4);
        T = [T; fscanf(mmfile,'%f')];
        if ( size(T) ~= 4*entries )
            message = ...
                str2mat('Data file does not contain expected amount of data.',...
                'Check that number of data lines matches nonzero count.');
            disp(message);
            error('Invalid data.');
        end
        T = reshape(T,4,entries)';
        A = sparse(T(:,1), T(:,2), T(:,3) + T(:,4)*sqrt(-1), rows , cols);
        
    elseif  ( strcmp(field,'pattern'))    % pattern matrix (no values given):
        
        T = fscanf(mmfile,'%f',2);
        T = [T; fscanf(mmfile,'%f')];
        if ( size(T) ~= 2*entries )
            message = ...
                str2mat('Data file does not contain expected amount of data.',...
                'Check that number of data lines matches nonzero count.');
            disp(message);
            error('Invalid data.');
        end
        T = reshape(T,2,entries)';
        A = sparse(T(:,1), T(:,2), ones(entries,1) , rows , cols);
        
    end
    
elseif ( strcmp(rep,'array') ) %  read matrix given in dense
    %  array (column major) format
    
    [sizeinfo,count] = sscanf(commentline,'%d%d');
    while ( count == 0 )
        commentline =  fgets(mmfile);
        if (commentline == -1 )
            error('End-of-file reached before size information was found.')
        end
        [sizeinfo,count] = sscanf(commentline,'%d%d');
        if ( count > 0 & count ~= 2 )
            error('Invalid size specification line.')
        end
    end
    rows = sizeinfo(1);
    cols = sizeinfo(2);
    entries = rows*cols;
    if  ( strcmp(field,'real') )               % real valued entries:
        A = fscanf(mmfile,'%f',1);
        A = [A; fscanf(mmfile,'%f')];
        if ( strcmp(symm,'symmetric') | strcmp(symm,'hermitian') | strcmp(symm,'skew-symmetric') )
            for j=1:cols-1,
                currenti = j*rows;
                A = [A(1:currenti); zeros(j,1);A(currenti+1:length(A))];
            end
        elseif ( ~ strcmp(symm,'general') )
            disp('Unrecognized symmetry')
            disp(symm)
            disp('Recognized choices:')
            disp('   symmetric')
            disp('   hermitian')
            disp('   skew-symmetric')
            disp('   general')
            error('Check symmetry specification in header.');
        end
        A = reshape(A,rows,cols);
    elseif  ( strcmp(field,'complex'))         % complx valued entries:
        tmpr = fscanf(mmfile,'%f',1);
        tmpi = fscanf(mmfile,'%f',1);
        A  = tmpr+tmpi*i;
        for j=1:entries-1
            tmpr = fscanf(mmfile,'%f',1);
            tmpi = fscanf(mmfile,'%f',1);
            A  = [A; tmpr + tmpi*i];
        end
        if ( strcmp(symm,'symmetric') | strcmp(symm,'hermitian') | strcmp(symm,'skew-symmetric') )
            for j=1:cols-1,
                currenti = j*rows;
                A = [A(1:currenti); zeros(j,1);A(currenti+1:length(A))];
            end
        elseif ( ~ strcmp(symm,'general') )
            disp('Unrecognized symmetry')
            disp(symm)
            disp('Recognized choices:')
            disp('   symmetric')
            disp('   hermitian')
            disp('   skew-symmetric')
            disp('   general')
            error('Check symmetry specification in header.');
        end
        A = reshape(A,rows,cols);
    elseif  ( strcmp(field,'pattern'))    % pattern (makes no sense for dense)
        disp('Matrix type:',field)
        error('Pattern matrix type invalid for array storage format.');
    else                                 % Unknown matrix type
        disp('Matrix type:',field)
        error('Invalid matrix type specification. Check header against MM documentation.');
    end
end

%
% If symmetric, skew-symmetric or Hermitian, duplicate lower
% triangular part and modify entries as appropriate:
%

if ( strcmp(symm,'symmetric') )
    A = A + A.' - diag(diag(A));
    entries = nnz(A);
elseif ( strcmp(symm,'hermitian') )
    A = A + A' - diag(diag(A));
    entries = nnz(A);
elseif ( strcmp(symm,'skew-symmetric') )
    A = A - A';
    entries = nnz(A);
end

fclose(mmfile);
end

function [y] = mv(M,x)
%MV Summary of this function goes here
%   Detailed explanation goes here
rows = size(M,1);
cols = size(M,2);
y = zeros(rows,1);
for i =1:rows
    for j = 1:cols
        y(i) = y(i)+M(i,j)*x(j);
    end
end
end

function [lambda_k,lambda_diff_vec,iter,lambda_k_vec] = power_iter(A,q,tol,opt_csr)
k_max=10^6;
N = length(q);
qk = zeros(N,1);
lambda_k = 0;
lambda_k_vec = zeros(k_max, 1);
lambda_diff_vec = zeros(k_max,1);
if opt_csr == 1
    zk = mv_csr(A,q);
    for k=1:k_max
        lambda_k_last = lambda_k;
        zk_norm = sqrt(dot_prod(zk,zk));
        for i=1:N
            qk(i) = zk(i)/zk_norm;
        end
        zk = mv_csr(A,qk);
        lambda_k = dot_prod(qk,zk);
        lambda_diff = abs(lambda_k-lambda_k_last);
        lambda_diff_vec(k) = lambda_diff;
        lambda_k_vec(k) = lambda_k;
        if lambda_diff < tol
            break
        end
    end
else
    zk = mv(A,q);
    for k=1:k_max
        lambda_k_last = lambda_k;
        zk_norm = sqrt(dot_prod(zk,zk));
        for i=1:N
            qk(i) = zk(i)/zk_norm;
        end
        zk = mv(A,qk);
        lambda_k = dot_prod(qk,zk);
        lambda_diff = abs(lambda_k-lambda_k_last);
        lambda_diff_vec(k) = lambda_diff;
        lambda_k_vec(k) = lambda_k;
        if lambda_diff < tol
            break
        end
    end
end
iter = k;
end

function T = lanczos(A,v,m)
n = length(v);
beta = zeros(m+1,1);
alfa = zeros(m,1);
vm1 = zeros(n,1);
for j=1:m
    w = mv_csr(A,v);
    for idx = 1:n
        w(idx) = w(idx)-beta(j)*vm1(idx);
    end
    alfa(j)=dot_prod(v,w);
    for idx = 1:n
        w(idx) = w(idx)-alfa(j)*v(idx);
    end
    beta(j+1)=sqrt(dot_prod(w,w));
    vm1 = v;
    for idx =1:n
        v(idx) = w(idx)/beta(j+1);
    end
end
T = zeros(m,m);
for i = 2:m-1
    T(i,i) = alfa(i);
    T(i,i-1) = beta(i);
    T(i,i+1) = beta(i+1);
end
T(1,1) = alfa(1);
T(1,2) = beta(2);
T(m,m) = alfa(m);
T(m,m-1) = beta(m);
end