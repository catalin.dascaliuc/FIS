clear all
clc
%% Test GS N=10, 100
use_cases = [10,100];
for idx = 1:length(use_cases)
    N = use_cases(idx);
    h = 1/N;
    x = 0:h:1;
    y = 0:h:1;
    u0 = zeros(N+1,N+1);
    f = zeros(N+1,N+1);
    u_star = zeros(N+1,N+1);
    for i = 1:N+1
        for j = 1:N+1
            f(i,j) = f_l((i-1)*h,(j-1)*h);
            u_star(i,j) = u_true((i-1)*h,(j-1)*h);
        end
    end
    nu = 10^9;
    tic
    [u,iter,err_vec,ro_vec] = GS(u0,f,nu,u_star);
    time_saved(idx) = toc;
    disp(['Use Case N=',num2str(use_cases(idx)),' finished'])
    figure;
    plot(err_vec, 'LineWidth', 1.5)
    title(['Error plot N = ',num2str(use_cases(idx)), ', iterations = ', num2str(length(err_vec)),', converged max error = ', num2str(err_vec(end))])
    ylabel('max_{i,j} |(u_{i,j})^{(\nu)} - u(x_i,y_j)|')
    xlabel('iterations')
    set(gca, 'FontName', 'Arial','FontSize', 11)
    figure;
    semilogy(ro_vec, 'LineWidth', 1.5)
    title(['Residual plot N = ',num2str(use_cases(idx))])
    ylabel('|u_\nu - u_{\nu-1}|_\infty')
    xlabel('iterations')
    set(gca, 'FontName', 'Arial','FontSize', 11)
end

%% Test RESTR
use_cases = [4,7];
for idx = 1:length(use_cases)
    N = 2^use_cases(idx);
    Nc = 2^(use_cases(idx)-1);
    h = 1/N;
    x = 0:h:1;
    y = 0:h:1;
    uh = zeros(N+1,N+1);
    for i = 1:N+1
        for j = 1:N+1
            uh(i,j) = u_true((i-1)*h,(j-1)*h);
        end
    end
    u2h = RESTR(uh,Nc);
    H = 2*h;
    max_err = 0;
    for i = 1:Nc-1
        for j = 1:Nc-1
            max_err_temp = abs(u2h(i+1,j+1)-u_true((i)*H,(j)*H));
            if max_err_temp > max_err
                max_err = max_err_temp;
            end
        end
    end
    err_restr(idx) = max_err;
end

%% Test Prolong
use_cases = [4,7];
for idx = 1:length(use_cases)
    N = 2^use_cases(idx);
    Nc = 2^(use_cases(idx)-1);
    h = 1/N;
    H = 1/Nc;
    x = 0:h:1;
    y = 0:h:1;
    u2h = zeros(Nc+1,Nc+1);
    for i = 1:Nc+1
        for j = 1:Nc+1
            u2h(i,j) = u_true((i-1)*H,(j-1)*H);
        end
    end
    uh = PROLONG(u2h,Nc);
    max_err = 0;
    for i = 1:N-1
        for j = 1:N-1
            max_err_temp = abs(uh(i+1,j+1)-u_true((i)*h,(j)*h));
            if max_err_temp > max_err
                max_err = max_err_temp;
            end
        end
    end
    err_prolong(idx) = max_err;
end
%% Main MG
n_cases = [4, 4, 7, 7];
nu1_cases = [1, 2, 1, 2];
nu2_cases = [1, 1, 1, 1];
gamma = 2;
for idx = 1:length(n_cases)
    n = n_cases(idx);
    nu1 = nu1_cases(idx);
    nu2 = nu2_cases(idx);
    m=0;
    l = n-1;
    N = 2^n;
    h = 1/N;
    ul_in = zeros(N+1,N+1);
    fl = zeros(N+1,N+1);
    u_star = zeros(N+1,N+1);
    r0_max = 0;
    for i = 1:N+1
        for j = 1:N+1
            fl(i,j) = f_l((i-1)*h,(j-1)*h);
            if abs(fl(i,j))>r0_max
                r0_max = abs(fl(i,j));
            end
            u_star(i,j) = u_true((i-1)*h,(j-1)*h);
        end
    end
    rel_vec = [];
    tic
    % MG iteration
    while 1
        m = m+1;
        ul_out = MG(l,ul_in,fl,gamma,nu1,nu2);
        ul_in = ul_out;
        rm = zeros(N+1,N+1);
        rm_max = 0;
        for i=1:N-1
            for j=1:N-1
                rm(i+1,j+1) = fl(i+1,j+1) + 1/h^2*(ul_out(i,j+1)-2*ul_out(i+1,j+1)+ul_out(i+2,j+1)) +...
                    1/h^2*(ul_out(i+1,j)-2*ul_out(i+1,j+1)+ul_out(i+1,j+2));
                if abs(rm(i+1,j+1)) > rm_max
                    rm_max = abs(rm(i+1,j+1));
                end
            end
        end
        relative_m = rm_max/r0_max;
        rel_vec(m) = relative_m;
        if relative_m < 10^-16 || m>=20
            break
        end
        
    end
    time_MG(idx) = toc;
    % error MG
    max_err = 0;
    for i = 1:N-1
        for j = 1:N-1
            max_err_temp = abs(ul_out(i+1,j+1)-u_star(i+1,j+1));
            if max_err_temp > max_err
                max_err = max_err_temp;
            end
        end
    end
    err_MG(idx) = max_err;
    figure;
    semilogy(0:length(rel_vec),[1 rel_vec], 'LineWidth', 1.5)
    title(['Relative residual plot n = ',num2str(n), ', \gamma = ', num2str(gamma),...
        ', \nu_1 = ',num2str(nu1),', \nu_2 = ',num2str(nu2)])
    ylabel('||r^{(m)}||_\infty/||r^{(0)}||_\infty')
    xlabel('iterations')
    set(gca, 'FontName', 'Arial','FontSize', 11)
end
%% Main MG investigate nu1
n_cases = [7, 7, 7, 7, 7];
% set a redundant initialization case, for some reason matlab is slower on the first
% iteration
nu1_cases = [1, 1, 2, 10, 100];
nu2_cases = [1, 1, 1, 1, 1];
gamma = 2;
for idx = 1:length(n_cases)
    n = n_cases(idx);
    nu1 = nu1_cases(idx);
    nu2 = nu2_cases(idx);
    m=0;
    l = n-1;
    N = 2^n;
    h = 1/N;
    ul_in = zeros(N+1,N+1);
    fl = zeros(N+1,N+1);
    u_star = zeros(N+1,N+1);
    r0_max = 0;
    for i = 1:N+1
        for j = 1:N+1
            fl(i,j) = f_l((i-1)*h,(j-1)*h);
            if abs(fl(i,j))>r0_max
                r0_max = abs(fl(i,j));
            end
            u_star(i,j) = u_true((i-1)*h,(j-1)*h);
        end
    end
    rel_vec = [];
    % MG iteration
    tic
    while 1
        m = m+1;
        ul_out = MG(l,ul_in,fl,gamma,nu1,nu2);
        ul_in = ul_out;
        rm = zeros(N+1,N+1);
        rm_max = 0;
        for i=1:N-1
            for j=1:N-1
                rm(i+1,j+1) = fl(i+1,j+1) + 1/h^2*(ul_out(i,j+1)-2*ul_out(i+1,j+1)+ul_out(i+2,j+1)) +...
                    1/h^2*(ul_out(i+1,j)-2*ul_out(i+1,j+1)+ul_out(i+1,j+2));
                if abs(rm(i+1,j+1)) > rm_max
                    rm_max = abs(rm(i+1,j+1));
                end
            end
        end
        relative_m = rm_max/r0_max;
        rel_vec(m) = relative_m;
        time_MG(m) = toc;
        if relative_m < 10^-16 || m>=20
            break
        end
    end
    
    % error MG
    max_err = 0;
    for i = 1:N-1
        for j = 1:N-1
            max_err_temp = abs(ul_out(i+1,j+1)-u_star(i+1,j+1));
            if max_err_temp > max_err
                max_err = max_err_temp;
            end
        end
    end
    err_MG(idx) = max_err;
    figure;
    semilogy([0 time_MG],[1 rel_vec], 'LineWidth', 1.5)
    title(['Relative residual plot n = ',num2str(n), ', \gamma = ', num2str(gamma),...
        ', \nu_1 = ',num2str(nu1),', \nu_2 = ',num2str(nu2)])
    ylabel('||r^{(m)}||_\infty/||r^{(0)}||_\infty')
    xlabel('Time in s')
    set(gca, 'FontName', 'Arial','FontSize', 11)
end
%% Functions
function z = u_true(x,y)
z = sin(2*pi*x)*sin(2*pi*y);
end
function z = f_l(x,y)
z = 8*pi^2*sin(2*pi*x)*sin(2*pi*y);
end
function [u,iter,err_vec,ro_vec] = GS(u0,f,nu,u_star)
N = size(u0,1)-1;
h = 1/N;
u = u0;
for idx = 1:nu
    u_old = u;
    ro = 0;
    max_err = 0;
    for j = 1:N-1
        for i = 1:N-1
            u(i+1,j+1) = 1/4*(h^2*f(i+1,j+1)+u(i,j+1)+u(i+2,j+1)+u(i+1,j)+u(i+1,j+2));
            ro_temp = abs(u(i+1,j+1)-u_old(i+1,j+1));
            max_err_temp = abs(u(i+1,j+1)-u_star(i+1,j+1));
            if  ro_temp > ro
                ro = ro_temp;
            end
            if max_err_temp > max_err
                max_err = max_err_temp;
            end
        end
    end
    err_vec(idx) = max_err;
    ro_vec(idx) = ro;
    if ro < 10^-10
        break
    end
end
iter = idx;
end
function u = Smooth(u0,f,nu)
% Gauss-Seidel smoothing
N = size(u0,1)-1;
h = 1/N;
u = u0;
for idx = 1:nu
    for j = 1:N-1
        for i = 1:N-1
            u(i+1,j+1) = 1/4*(h^2*f(i+1,j+1)+u(i,j+1)+u(i+2,j+1)+u(i+1,j)+u(i+1,j+2));
        end
    end
end
end
function u2h = RESTR(uh,Nc)
u2h = zeros(Nc+1,Nc+1);
for i = 1:Nc-1
    ii = 2*i;
    for j = 1:Nc-1
        jj=2*j;
        u2h(i+1,j+1) = 1/16*(uh(ii,jj)+2*uh(ii+1,jj)+...
            uh(ii+2,jj)+2*uh(ii,jj+1)+4*uh(ii+1,jj+1)+2*uh(ii+2,jj+1)+...
            uh(ii,jj+2)+2*uh(ii+1,jj+2)+uh(ii+2,jj+2));
    end
end
end
function uh = PROLONG(u2h,Nc)
uh = zeros(Nc*2+1,Nc*2+1);
for i=1:Nc-1
    ii=2*i;
    for j=1:Nc-1
        jj = 2*j;
        uh(ii,jj) = uh(ii,jj)+1/4*u2h(i+1,j+1);
        uh(ii+1,jj) = uh(ii+1,jj)+1/2*u2h(i+1,j+1);
        uh(ii+2,jj) = uh(ii+2,jj)+1/4*u2h(i+1,j+1);
        uh(ii,jj+1) = uh(ii,jj+1)+1/2*u2h(i+1,j+1);
        uh(ii+1,jj+1) = uh(ii+1,jj+1) + u2h(i+1,j+1);
        uh(ii+2,jj+1) = uh(ii+2,jj+1) + 1/2*u2h(i+1,j+1);
        uh(ii,jj+2) = uh(ii,jj+2) + 1/4*u2h(i+1,j+1);
        uh(ii+1,jj+2) = uh(ii+1,jj+2) + 1/2*u2h(i+1,j+1);
        uh(ii+2,jj+2) = uh(ii+2,jj+2) + 1/4*u2h(i+1,j+1);
    end
end
end

function ul_out = MG(l,ul_in,fl,gamma,nu1,nu2)
N = size(ul_in,1)-1;
h = 1/N;
% smoothing
ul = Smooth(ul_in,fl,nu1);
% residual calculation
rl = zeros(N+1,N+1);
for i=1:N-1
    for j=1:N-1
        rl(i+1,j+1) = fl(i+1,j+1) + 1/h^2*(ul(i,j+1)-2*ul(i+1,j+1)+ul(i+2,j+1)) +...
            1/h^2*(ul(i+1,j)-2*ul(i+1,j+1)+ul(i+1,j+2));
    end
end
Nc = N/2;
rl_1 = RESTR(rl,Nc);
if l == 1
    el_0 = zeros(Nc+1,Nc+1);
    el_1 = Smooth(el_0,-rl_1,1);
else
    el_1 = zeros(Nc+1,Nc+1);
    for j = 1:gamma
        el_1 = MG(l-1,el_1,-rl_1,gamma,nu1,nu2);
    end
end
el = PROLONG(el_1,Nc);
ul_out = Smooth(ul-el,fl,nu2);
end

